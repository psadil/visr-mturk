async function gen_demographics(){
  let timeline = [];

  let demographics_explain = {
    type: 'html-button-response',
    stimulus: `<section class='instructions'><p>That's the end of the experiment!</p>
    <p>The National Institude of Health requests that basic demographic information (sex at birth, ethnicity, race, and age),
    for clinical or behavioral studies, to the extent that this information is provided by research participants.</p>
    <p>You are under no obligation to provide this information.</p>
    <p>If you would rather not answer these questions, you will still receive full compensation for your participation in this study,
    and the data you provided will still be useful for our research.</p>
    </section>`,
    choices: ['continue'],
    on_load: function(){
      btnSwapper(1000);
    }
  };
  timeline.push(demographics_explain)

  let sex_at_birth = {
    type: 'html-button-response',
    stimulus: `<section class='instructions'><p>Sex at birth?</p> </section>`,
    choices: ['Female', 'Male', 'Other', 'Rather not say'],
    on_finish: function(data) {
        psiturk.recordUnstructuredData('sex_at_birth', data.button_pressed);
    }
  };
  timeline.push(sex_at_birth)

  let ethnicity = {
    type: 'html-button-response',
    stimulus: `<section class='instructions'><p>Ethnicity?</p> </section>`,
    choices: ['Hispanic or Latino', 'Not Hispanic or Latino', 'Rather not say'],
    on_finish: function(data) {
        psiturk.recordUnstructuredData('ethnicity', data.button_pressed);
    }
  };
  timeline.push(ethnicity)

  let race = {
    type: 'html-button-response',
    stimulus: `<section class='instructions'><p>Race?</p> </section>`,
    choices: ['American Indian/Alaska Native', 'Asian', 'Native Hawaiian or Other Pacific Islander', 'Black or African American', 'White', 'Rather not say'],
    on_finish: function(data) {
        psiturk.recordUnstructuredData('race', data.button_pressed);
    }
  };
  timeline.push(race)

  let age = {
    type: 'html-autocomplete-response',
    stimulus: `<section class='instructions'><p>Age?</p>
               <p>(You may enter 'Rather not say')</p></section>`,
    choices: _.chain(_.range(0, 120))
              .map(a=>_.object([['data', a.toString()], ['value', a.toString()]]))
              .value()
              .concat({'data':'Rather not say', 'value': 'Rather not say'}),
    on_finish: function(data) {
        psiturk.recordUnstructuredData('age', data.button_pressed);
    },
  };
  timeline.push(age)

  return timeline;
};
