
function AssertException(message) { this.message = message; }
AssertException.prototype.toString = function () {
	return 'AssertException: ' + this.message;
};

function assert(exp, message) {
	if (!exp) {
		throw new AssertException(message);
	}
}

// Mean of booleans (true==1; false==0)
function boolpercent(arr) {
	var count = 0;
	for (var i=0; i<arr.length; i++) {
		if (arr[i]) { count++; }
	}
	return 100* count / arr.length;
}

async function load_csv(file){
  let csv_fetched = await fetch(file);
  let csv = await csv_fetched.text();
  let objectNames = await Papa.parse(csv, {header: true, dynamicTyping: true});
  // let objectNames = await Papa.parse("static/js/objectNames_2afc.csv", {header: true});
  return objectNames.data;
};

async function index_array_by_array(array, index){
	let out = [];
	for(ind in index){
		out.push(array[index[ind]]);
	}
  return out;
};
/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 https://stackoverflow.com/questions/1527803/generating-random-whole-numbers-in-javascript-in-a-specific-range#1527820
 */
async function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function btnSwapper(duration=1000){
	$('.jspsych-btn').prop('disabled', true);
	setTimeout(function(){
		$('.jspsych-btn').prop('disabled', false);
	}, duration);
}

function moveBtnToBottom(){
	$('#jspsych-image-button-response-btngroup').css({'position': 'absolute',
		                                                'top': '80%',
                                                    'left': '50%',
                                                    'margin-right': '-50%',
                                                    'transform': 'translate(-50%, -50%)'
																										});
}
// somethings I don't understay about why this is necessary!
function adjust2AFC(){
	$('#jspsych-image-button-response-stimulus_right2').css({'position': 'absolute',
																														'width': '30vw',
																														'height': '30vw',
																														'top': '20%',
																														'right': '20%',
																														'border':'3px solid transparent',
																														'padding':'3vw'
																													 });
}
