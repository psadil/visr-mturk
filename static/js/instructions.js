async function gen_instructions_study(){
  let timeline = [];

  let instructions = {
    type: 'html-button-response',
    stimulus: `<section class='instructions'><p>Please study the following objects. They will be presented as either an image or word.</p>
    <p>Your memory of the objects will then be tested.</p>
    <p>To help you study, you will be asked to answer a brief question about each object referenced by an image or word.</p> </section>`,
    choices: ['start study phase'],
    on_load: function(){
      btnSwapper(3000);
    }
  };
  timeline.push(instructions)

  return timeline;
};

async function gen_instructions_afc(){
  let timeline = [];

  let instructions_2afc = {
    type: 'html-button-response',
    stimulus: `<section class='instructions'><p>That's all of the objects. You have reached the memory test!</p>
    <p>You will see two pairs of object parts. The parts come from partially hidden objects, some of which you may have studied.</p>
    <p>One pair will contain parts from a single object, but the other will contain parts from two different objects.</p>
    <p>Your task will be to choose the pair of parts that come from the same object.</p>
    <p>Some of the parts will come from objects that you studied and some will be new.</p>
    <p>You will have only 5 seconds to make a response. Please try to answer the question correctly, but always prioritize answering in under 5 seconds.</p>
    <p>Press continue to see an example.</p> </section>`,
    choices: ['continue'],
    on_load: function(){
      btnSwapper(5000);
    }
  };
  timeline.push(instructions_2afc)

  let demo_2afc = {
    type: '2afc-button-response',
    stimulus: ['static/images/objects/png/object-212_paired-215_ap-1.png',
               'static/images/objects/png/object-212_paired-215_ap-2.png',
               'static/images/objects/png/object-212_paired-215_ap-1.png',
               'static/images/objects/png/object-215_paired-212_ap-3.png'],
    prompt: `<section class="centerTop"><p>Which pair comes from the same object?</p>
    <p>Click on the pair to respond.</p></section>`,
    choices: ['left', 'right'],
    trial_duration: 5000,
    on_load: function() {
      btnSwapper(1500);
      moveBtnToBottom();
      adjust2AFC();
    }
  };
  timeline.push(demo_2afc)

  let instructions_finish = {
    type: 'html-button-response',
    stimulus: `<section class='instructions'><p>The correct choice was the pair on the left.</p>
    <p>Please try to respond quickly and accurately.</p>
    <p>The testing phase will begin when you press 'continue'</p> </section>`,
    choices: ['continue']
  };
  timeline.push(instructions_finish);
  return timeline;
};

async function gen_instructions_name(){
  let timeline = [];

  let instructions_name = {
    type: 'html-button-response',
    stimulus: `<section class='instructions'><p>You have now reached the second (and final) kind of memory test!</p>
    <p>You will be shown just one of the object parts and asked to identify the object to which the part belongs.</p>
    <p>Press continue to see an example.</p></section>`,
    choices: ['continue'],
    on_load: function(){
      btnSwapper(3000);
    }
  };
  timeline.push(instructions_name)

  let demo_name = {
    type: 'image-textbox-response',
    stimulus: 'static/images/objects/png/object-212_paired-215_ap-1.png',
    prompt: `<section class="centerTop"><p>Which object is this a part of?</p>
    <p>Type your response and press</p>
    <p>'Enter' or 'Return' when finished</p></section>`,
    on_load: function(){
      $('#image-textbox-stimulus').css({'background': 'rgb(161, 161, 161)',
                                             'width': '30vw',
                                             'height': '30vw',
                                             'border': '3vw solid white'});
                                             }
    }

  timeline.push(demo_name)

  let instructions_finish = {
    type: 'html-button-response',
    stimulus: `<section class='instructions'><p>That was a part of a 'light switch'.</p>
    <p>These questions do not have a time limit, so please respond as accurately as you can.</p>
    <p>The testing phase will begin when you press 'continue'</p> </section>`,
    choices: ['continue']
  };
  timeline.push(instructions_finish)

  return timeline;
};
