/* load psiturk */
var psiturk = new PsiTurk(uniqueId, adServerLoc, mode);

async function gen_timeline(){
  let timeline = [];
  let instructions_study = await gen_instructions_study();
  let trials_study = await gen_timeline_variables_study();
  let instructions_afc = await gen_instructions_afc();
  let trials_afc = await gen_timeline_variables_afc();
  let instructions_name = await gen_instructions_name();
  let trials_name = await gen_timeline_variables_name();
  let demographic_questions = await gen_demographics();

  var pb = 0;
  var step = 1/(trials_study.length+(trials_afc.length + trials_name.length));

  timeline.push({
    type: 'fullscreen',
    fullscreen_mode: true,
    message: `<p class = "instructions">The experiment will switch to full screen mode when you press the button below</p>`
  });

  let welcome_block = {
    type: 'html-button-response',
    stimulus: "<p class = 'instructions'>Welcome to the experiment</p>",
    choices: ['begin']
  };

  timeline.push(welcome_block);
  instructions_study.forEach(a => timeline.push(a));
  let fixation = {
    type: 'html-keyboard-response',
    stimulus: '<div style="font-size:60px;color:white">+</div>',
    choices: jsPsych.NO_KEYS,
    trial_duration: 1000
  };

  let study = {
    type: jsPsych.timelineVariable('type'),
    stimulus: jsPsych.timelineVariable('stimulus'),
    choices: jsPsych.NO_KEYS,
    trial_duration: jsPsych.timelineVariable('trial_duration'),
    on_load: function(){
      $('#jspsych-image-keyboard-response-stimulus').css({'background': 'rgb(161, 161, 161)',
                                                          'width': '30vw',
                                                          'height': '30vw',
                                                          'border': '3vw solid white'});
                                                        }
              };

  let study_question = {
    type: 'html-button-response',
    stimulus: jsPsych.timelineVariable('study_question'),
    choices: ['yes', 'no'],
    data: jsPsych.timelineVariable('data'),
    margin_vertical: '25px',
    on_load: function(){
      btnSwapper(1000);
    },
    on_finish: function(data) {
        psiturk.recordTrialData(data);
        pb+=step;
        jsPsych.setProgressBar(pb);
    },
  }

  let study_procedure = {
    timeline: [fixation, study, study_question],
    timeline_variables: trials_study
  };
  timeline.push(study_procedure);
  instructions_afc.forEach(a => timeline.push(a));
  let afc = {
    type: '2afc-button-response',
    prompt: '<div class="centerTop">Which pair comes from the same object?</div>',
    choices: ['left', 'right'],
    stimulus: jsPsych.timelineVariable('stimulus'),
    trial_duration: 5000,
    on_load: function() {
      btnSwapper(1500);
      adjust2AFC();
    },
    on_finish: function(data) {
      psiturk.recordTrialData(data);
      pb+=step;
      jsPsych.setProgressBar(pb);
      if (data.button_pressed == null) {
        data.give_feedback = true;
      }else{
        data.give_feedback = false;
      }
    }
  };

  var afc_feedback = {
    type: 'html-keyboard-response',
    choices: jsPsych.NO_KEYS,
    stimulus: `<p class="wordstudy">ERROR! Please respond quicker.</p>`,
    trial_duration: 5000
  };

  var afc_feedback_node = {
    timeline: [afc_feedback],
    conditional_function: function(){
      return jsPsych.data.get().last(1).values()[0].give_feedback;
    }
  };

  let afc_procedure = {
    timeline: [afc, afc_feedback_node],
    timeline_variables: trials_afc
  };
  timeline.push(afc_procedure);

  instructions_name.forEach(a => timeline.push(a));
  let name = {
    type: 'image-textbox-response',
    prompt: '<div class="centerTop">Which object is this part of?</div>',
    timeline: trials_name,
    on_load: function() {
      $('#image-textbox-stimulus').css({'background': 'rgb(161, 161, 161)',
                                             'width': '30vw',
                                             'height': '30vw',
                                             'border': '3vw solid white'});
                                             },
    on_finish: function(data) {
        psiturk.recordTrialData(data);
        pb+=step;
        jsPsych.setProgressBar(pb);
    },
  };
  timeline.push(name);

  demographic_questions.forEach(a => timeline.push(a));

  timeline.push({
    type: 'fullscreen',
    fullscreen_mode: false
  });

  return timeline;
};

/* start the experiment */
async function startExperiment(){

  let timeline = await gen_timeline();

    jsPsych.init({
        timeline: timeline,
        preload_images: _.chain(timeline[3].timeline_variables).filter(a=>a.type=='image-keyboard-response').map(a=>a.stimulus).value(),
        show_progress_bar: true,
        auto_update_progress_bar: false,
        on_finish: function() {
          psiturk.saveData({
              success: function() {
                      psiturk.completeHIT();
              }
          });
        },
    });
}

startExperiment();
