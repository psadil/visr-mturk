async function gen_timeline_variables_study(){
  let trials = [];
  let study_questions_img = ['<p class="wordstudy">Was that object symmetric?</p>',
                             '<p class="wordstudy">Was that a narrow object?</p>',
                             '<p class="wordstudy">Was that object reflective?</p>',
                             '<p class="wordstudy">Did that object contain multiple parts?</p>'];
  let study_questions_word = ['<p class="wordstudy">Would that object be symmetric?</p>',
                              '<p class="wordstudy">Was that a narrow object?</p>',
                              '<p class="wordstudy">Would that object be reflective?</p>',
                              '<p class="wordstudy">Does that object contain multiple parts?</p>'];

  let study = await load_csv('static/csv/study.csv');
  let condition = psiturk.taskdata.get('condition');
  let counterbalance = psiturk.taskdata.get('counterbalance');

  let list_raw = _.chain(study)
                  .filter(row => (row.cond == condition) & (row.list == counterbalance ))
                  .value();
  let first = _.chain(list_raw)
               .filter(row => row.rep == 1)
               .shuffle()
               .value();
  let second = _.chain(list_raw)
                .filter(row => row.rep == 2)
                .shuffle()
                .value();
  let third = _.chain(list_raw)
                .filter(row => row.rep == 3)
                .shuffle()
                .value();
  let list = first.concat(second).concat(third);
   for(let t = 0; t < list.length; t++){
     if(list[t].study_type == 'word'){
       trials.push({stimulus: '<p class="wordstudy">' + list[t].name1 + '</p>'
       , type: 'html-keyboard-response'
       , study_question: study_questions_word[list[t].study_question]
       , trial_duration: 1000
       , data: {choices: ['yes', 'no']} });
     } else if (list[t].study_type == 'image') {
       trials.push({stimulus: `static/images/objects/png/object-${list[t].object.toString().padStart(3,'0')}_paired-${list[t].pair.toString().padStart(3,'0')}_ap-0.png`
       , type: 'image-keyboard-response'
       , study_question: study_questions_img[list[t].study_question]
       , trial_duration: 1000
       , data: {choices: ['yes', 'no'] } });
     }
   }

  return trials;
};

async function gen_timeline_variables_afc(){
  let trials = [];

  let test = await load_csv('static/csv/test.csv');
  let condition = psiturk.taskdata.get('condition');
  let counterbalance = psiturk.taskdata.get('counterbalance');

  let list = _.chain(test)
  .filter(row => (row.cond == condition & row.list == counterbalance ))
  .shuffle()
  .value();

  let left = true;
  for(let t = 0; t < list.length; t++){
    let base = `static/images/objects/png/object-${list[t].object.toString().padStart(3,'0')}_paired-${list[t].pair.toString().padStart(3,'0')}`;
    let pair = `static/images/objects/png/object-${list[t].pair.toString().padStart(3,'0')}_paired-${list[t].object.toString().padStart(3,'0')}`;

    left = Math.random() >= 0.5;
    if (left){
      trials.push( {stimulus: [`${base}_ap-1.png`, `${base}_ap-2.png`,
                                   `${base}_ap-1.png`, `${pair}_ap-3.png`]});
    } else{
      trials.push( {stimulus: [`${base}_ap-1.png`, `${pair}_ap-3.png`,
                                   `${base}_ap-1.png`, `${base}_ap-2.png`]});
    }
  }

  return trials;
};

async function gen_timeline_variables_name(){
  let trials = [];

  let test = await load_csv('static/csv/test.csv');
  let condition = psiturk.taskdata.get('condition');
  let counterbalance = psiturk.taskdata.get('counterbalance');

  let list = _.chain(test)
  .filter(row => (row.cond == condition & row.list == counterbalance ))
  .shuffle()
  .value();

  let left = true;
  for(let t = 0; t < list.length; t++){
    trials.push( {stimulus: `static/images/objects/png/object-${list[t].object.toString().padStart(3,'0')}_paired-${list[t].pair.toString().padStart(3,'0')}_ap-1.png`});
  }

  return trials;
};
