/**
 * image-autocomplete-response
 * Patrick Sadil
 *
 * plugin for displaying a stimulus and autocompleting response_ends_trial
 * requires https://github.com/devbridge/jQuery-Autocomplete
 *
 * documentation: docs.jspsych.org
 *
 **/

jsPsych.plugins["image-textbox-response"] = (function() {

  var plugin = {};

  jsPsych.pluginAPI.registerPreload('image-textbox-response', 'stimulus', 'image');

  plugin.info = {
    name: 'image-textbox-response',
    description: '',
    parameters: {
      stimulus: {
        type: jsPsych.plugins.parameterType.IMAGE,
        pretty_name: 'Stimulus',
        default: undefined,
        description: 'The image to be displayed'
      },
      prompt: {
        type: jsPsych.plugins.parameterType.STRING,
        pretty_name: 'Prompt',
        default: null,
        description: 'Any content here will be displayed under the button.'
      },
      stimulus_duration: {
        type: jsPsych.plugins.parameterType.INT,
        pretty_name: 'Stimulus duration',
        default: null,
        description: 'How long to hide the stimulus.'
      },
      trial_duration: {
        type: jsPsych.plugins.parameterType.INT,
        pretty_name: 'Trial duration',
        default: null,
        description: 'How long to show the trial.'
      },
      response_ends_trial: {
        type: jsPsych.plugins.parameterType.BOOL,
        pretty_name: 'Response ends trial',
        default: true,
        description: 'If true, then trial will end when user responds.'
      },
    }
  }

  plugin.trial = function(display_element, trial) {

    if(typeof trial.stimulus === 'undefined'){
      console.error('Required parameter "stimulus" missing in image-autocomplete-response');
    }

    // display stimulus
    var html = `<img src=${trial.stimulus} id="image-textbox-stimulus"></img>`;

    // display autocomplete bar
    // all of the logic about what to do next happens within the autocomplete onSelect
    // value.
    html += `<input autofocus required type="text" name="image-textbox-response-text" id="image-textbox-response-text">`

    //show prompt if there is one
    if (trial.prompt !== null) {
      html += trial.prompt;
    }

    display_element.innerHTML = html;

    // allow for submission with press of enter key
    // only submit when response is valid. regadless, because we want, not necessarily
    // the values but instead a single code (suggestion.data), the Autocomplete
    // select method is run, rather than form submit
    $("#image-textbox-response-text").keyup(function(event) {
      if (event.keyCode === 13) {
        after_response($('#image-textbox-response-text').val());
      }
    });
    // need to override this value to suppress native browser autocompletion
    $('#image-textbox-response-text').attr( "autocomplete", "off" );
    $('#image-textbox-response-text').focus();

    // start timing
    var start_time = Date.now();

    // store response
    var response = {
      rt: null,
      input: null
    };

    // function to handle responses by the subject
    function after_response(input) {

      // measure rt
      var end_time = Date.now();
      var rt = end_time - start_time;
      response.input = input;
      response.rt = rt;

      if (trial.response_ends_trial) {
        end_trial();
      }
    };

    // function to end trial when it is time
    function end_trial() {

      // kill any remaining setTimeout handlers
      jsPsych.pluginAPI.clearAllTimeouts();

      // gather the data to store for the trial
      var trial_data = {
        "rt": response.rt,
        "stimulus": trial.stimulus,
        "input": response.input
      };

      // clear the display
      display_element.innerHTML = '';

      // move on to the next trial
      jsPsych.finishTrial(trial_data);
    };


    // hide image if timing is set
    if (trial.stimulus_duration !== null) {
      jsPsych.pluginAPI.setTimeout(function() {
        display_element.querySelector('#image-textbox-response-stimulus').style.visibility = 'hidden';
      }, trial.stimulus_duration);
    }

    // end trial if time limit is set
    if (trial.trial_duration !== null) {
      jsPsych.pluginAPI.setTimeout(function() {
        end_trial();
      }, trial.trial_duration);
    }

  };

  return plugin;
})();
