/**
 * image-autocomplete-response
 * Patrick Sadil
 *
 * plugin for displaying a stimulus and autocompleting response_ends_trial
 * requires https://github.com/devbridge/jQuery-Autocomplete
 *
 * documentation: docs.jspsych.org
 *
 **/

jsPsych.plugins["image-autocomplete-response"] = (function() {

  var plugin = {};

  jsPsych.pluginAPI.registerPreload('image-autocomplete-response', 'stimulus', 'image');

  plugin.info = {
    name: 'image-autocomplete-response',
    description: '',
    parameters: {
      stimulus: {
        type: jsPsych.plugins.parameterType.IMAGE,
        pretty_name: 'Stimulus',
        default: undefined,
        description: 'The image to be displayed'
      },
      choices: {
        type: jsPsych.plugins.parameterType.STRING,
        pretty_name: 'Choices',
        default: undefined,
        array: true,
        description: 'The array of objects to choose from.'
      },
      prompt: {
        type: jsPsych.plugins.parameterType.STRING,
        pretty_name: 'Prompt',
        default: null,
        description: 'Any content here will be displayed under the button.'
      },
      stimulus_duration: {
        type: jsPsych.plugins.parameterType.INT,
        pretty_name: 'Stimulus duration',
        default: null,
        description: 'How long to hide the stimulus.'
      },
      trial_duration: {
        type: jsPsych.plugins.parameterType.INT,
        pretty_name: 'Trial duration',
        default: null,
        description: 'How long to show the trial.'
      },
      response_ends_trial: {
        type: jsPsych.plugins.parameterType.BOOL,
        pretty_name: 'Response ends trial',
        default: true,
        description: 'If true, then trial will end when user responds.'
      },
    }
  }

  plugin.trial = function(display_element, trial) {

    if(typeof trial.choices === 'undefined'){
      console.error('Required parameter "choices" missing in image-autocomplete-response');
    }
    if(typeof trial.stimulus === 'undefined'){
      console.error('Required parameter "stimulus" missing in image-autocomplete-response');
    }

    // display stimulus
    var html = `<img src=${trial.stimulus} id="image-autocomplete-stimulus"></img>`;

    // display autocomplete bar
    // all of the logic about what to do next happens within the autocomplete onSelect
    // value.
    html += `<input autofocus required type="text" name="image-autocomplete-response-text" id="image-autocomplete-response-text">`

    //show prompt if there is one
    if (trial.prompt !== null) {
      html += trial.prompt;
    }

    display_element.innerHTML = html;

    // construct autocomplete details
    // note that we need both the onSelect event here as well as the if/then control
    // during the submission of the form. onSelect refers to the clicking of
    // a suggestion with the mouse (or tab completion), whereas the submit event
    // refers to pressing of enter
    $('#image-autocomplete-response-text').autocomplete({
    	lookup: trial.choices,
      maxHeight: 100,
      showNoSuggestionNotice: true,
      triggerSelectOnValidInput: false,
      noSuggestionNotice: `<p>not a valid option</p>`,
    	onSelect: function (suggestion) {
        after_response(suggestion.data);
      }
    });

    // allow for submission with press of enter key
    // only submit when response is valid. regadless, because we want, not necessarily
    // the values but instead a single code (suggestion.data), the Autocomplete
    // select method is run, rather than form submit
    $("#image-autocomplete-response-text").keyup(function(event) {
      if (event.keyCode === 13) {
        let currentresp = $('#image-autocomplete-response-text').val();
        if ( _.chain(trial.choices).map(suggestion => suggestion.value).contains(currentresp).value()) {
          $('#image-autocomplete-response-text').autocomplete().select(0);
        }else{
          $('#image-autocomplete-response-text').autocomplete().noSuggestions();
        }
      }
    });
    // need to override this value to suppress native browser autocompletion
    $('#image-autocomplete-response-text').attr( "autocomplete", "off" );

    // start timing
    var start_time = Date.now();

    // store response
    var response = {
      rt: null,
      button: null
    };

    // function to handle responses by the subject
    function after_response(choice) {

      // measure rt
      var end_time = Date.now();
      var rt = end_time - start_time;
      response.button = choice;
      response.rt = rt;

      // after a valid response disable autocomplete box
      $('#autocomplete').autocomplete('disable');

      if (trial.response_ends_trial) {
        end_trial();
      }
    };

    // function to end trial when it is time
    function end_trial() {

      // kill any remaining setTimeout handlers
      jsPsych.pluginAPI.clearAllTimeouts();

      // gather the data to store for the trial
      var trial_data = {
        "rt": response.rt,
        "stimulus": trial.stimulus,
        "button_pressed": response.button
      };

      // clear the display
      display_element.innerHTML = '';

      // move on to the next trial
      jsPsych.finishTrial(trial_data);
    };


    // hide image if timing is set
    if (trial.stimulus_duration !== null) {
      jsPsych.pluginAPI.setTimeout(function() {
        display_element.querySelector('#image-autocomplete-response-stimulus').style.visibility = 'hidden';
      }, trial.stimulus_duration);
    }

    // end trial if time limit is set
    if (trial.trial_duration !== null) {
      jsPsych.pluginAPI.setTimeout(function() {
        end_trial();
      }, trial.trial_duration);
    }

  };

  return plugin;
})();
