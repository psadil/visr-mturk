## When debugging locally

In config.txt
 - host = localhost
 - database_url = sqlite:///tmp.db

Setup the necessary python environment (conda environment named python27, running python 2.7)
```bash
source activate python27
```

in bash
```bash
psiturk
server on
debug
```

## Debugging in sandbox

In config.txt
 - number_hits_approved = 0
 - allow_repeats = true
 - host = 0.0.0.0
 - database_url = postgres://pdipocnvcdkezp:a0410fbed6f1ff85c2b73d13305bf0259bafa939d46744257e0fcdc54d863230@ec2-54-227-241-179.compute-1.amazonaws.com:5432/d4kueqmunhql3n


Push master branch to gitlab
- will autodeploy to heroku (give it a couple of minutes)

Setup the necessary python environment
```bash
source activate python27
```

in bash (note _lack_ of server on!)
```bash
psiturk
hit create n_participants reward duration
```

Follow provided link and go through experiment. Can safely close psiturk and bash
without disrupting experiment.

### Gotcha's
 - will take a minute or two to upload to the sandbox
 - will also take a minute or two before a worker that as submitted the hit is registered as such
 - the number of hits in the psiturk console won't be incremented until you restart it
 - no need to turn the server on!

## Deploy

in config.txt
 - number_hits_approved = 100
 - allow_repeats = false
 - increment version number in config.txt from
 whatever is currently listed at https://dashboard.heroku.com/apps/blooming-hamlet-66425/activity

in bash
```bash
psiturk
mode
hit create n_participants reward duration
```

Only difference in bash is that mode is switched to live while in psiturk command line

### Gotcha's
 - If in free mode, the app sleeps after ~30 minutes. Although this is somewhat
 useful (it doesn't use up the free dyno hours), awakening takes ~10 seconds. That
 can be annoying for turk workers, so you might want to follow the link to the ad
 immediately after creating the hit (awakening the dyno before a worker responds)
 - CAUTION: psiturk deploys the experiment based on whatever the current values
 present in config.txt, regardless of the local or remote branch. This means that
 if, e.g., the config file says to use the ad server, psiturk will try to use the ad
 server. STOP editing files locally after reaching this point!
